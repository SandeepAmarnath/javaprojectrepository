package myPackage;

import java.util.Arrays;

public class QuickSortAlgorithm {

	static int[] a = new int[] {10, 16, 8, 12, 15, 6, 3, 9};
	
	public static void main(String[] args) {
		
		quick(0,7);
		System.out.println(Arrays.toString(a));
	}
	
	
	public static int partition(int l, int h) {
		
		
		int i=l;
		int j=h;
		
		
		while(i<j) {
			
			
			
			do {
				i++;
			}while(a[i]<a[l]);
			
			while(a[j]>a[l]) {
				j--;
			}
			
			if(i<j) {
				
				int temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
			
			
			
		}
		
		int temp = a[l];
		a[l] = a[j];
		a[j] = temp;
		return j;
		
		
		
	}
	
	
	
	public static void quick(int l, int h) {
		
		if(l<h) {
			
			int j = partition(l,h);
			quick(l,j);
			quick(j+1,h);
			
			
		}
		
	}
	
	
}
